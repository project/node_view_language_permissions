<?php

namespace Drupal\Tests\node_view_language_permissions\Functional;

use Drupal\Tests\BrowserTestBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Tests that the condition plugins work.
 *
 * @group node_view_language_permissions
 */
class NodeViewLanguagePermissionsTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node_view_language_permissions',
    'content_moderation',
  ];

  /**
   * Default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Use the standard profile.
   *
   * @var string
   */
  protected $profile = 'minimal';

  /**
   * Admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $admin;

  /**
   * Auth user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $user;

  /**
   * Node one.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node1;

  /**
   * Node two.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node2;

  /**
   * Node three.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node3;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->admin = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($this->admin);
    $this->user = $this->drupalCreateUser([], NULL, FALSE);

    // Create content types.
    $this->createContentType([
      'name' => 'Page',
      'type' => 'page',
    ]);
    $this->createContentType([
      'name' => 'Article',
      'type' => 'article',
    ]);

    // Disable translation download.
    $this->drupalGet('/admin/config/regional/translate/settings');
    $this->submitForm([
      'use_source' => 'local',
    ], 'Save configuration');

    // Create FR.
    $this->drupalGet('/admin/config/regional/language/add');
    $this->submitForm([
      'predefined_langcode' => 'fr',
    ], 'Add language');

    // Set prefixes to en and fr.
    $this->drupalGet('/admin/config/regional/language/detection/url');
    $this->submitForm([
      'prefix[en]' => 'en',
      'prefix[fr]' => 'fr',
    ], 'Save configuration');

    // Set up URL and language selection page methods.
    $this->drupalGet('/admin/config/regional/language/detection');
    $this->submitForm([
      'language_interface[enabled][language-url]' => FALSE,
      'language_content[configurable]' => TRUE,
      'language_content[enabled][language-url]' => TRUE,
    ], 'Save settings');

    // Turn on content translation for pages.
    $this->drupalGet('/admin/structure/types/manage/page');
    $this->submitForm([
      'language_configuration[content_translation]' => TRUE,
    ], 'Save');
    $this->drupalGet('/admin/structure/types/manage/article');
    $this->submitForm([
      'language_configuration[content_translation]' => TRUE,
    ], 'Save');

    // Create nodes.
    $this->node1 = $this->createNode([
      'title' => 'Node one',
      'type' => 'article',
    ]);
    $this->node2 = $this->createNode([
      'title' => 'Node two',
      'type' => 'page',
    ]);
    $this->node3 = $this->createNode([
      'title' => 'Node three',
      'type' => 'article',
    ]);

    // Translate nodes.
    $this->drupalGet('/fr/node/' . $this->node1->id() . '/translations/add/en/fr');
    $this->submitForm([
      'title[0][value]' => 'Nodule une',
    ], 'Save (this translation)');

    $this->drupalGet('/fr/node/' . $this->node2->id() . '/translations/add/en/fr');
    $this->submitForm([
      'title[0][value]' => 'Nodule deux',
    ], 'Save (this translation)');

    $this->drupalGet('/fr/node/' . $this->node3->id() . '/translations/add/en/fr');
    $this->submitForm([
      'title[0][value]' => 'Nodule trois',
    ], 'Save (this translation)');

    // Set permissions.
    $this->drupalGet('/admin/people/permissions');
    $this->submitForm([
      'authenticated[view any article en content]' => TRUE,
      'authenticated[view any page fr content]' => TRUE,
    ], 'Save permissions');
    $this->drupalGet('admin/reports/status/rebuild');
    $this->submitForm([], 'Rebuild permissions');

    $this->drupalLogout();
  }

  /**
   * All tests.
   */
  public function testNodeViewLanguagePermissions() {
    $this->subTestLoggedOut();
    $this->subTestLoggedIn();
    $this->subTestUnpublished();
    $this->subTestUnpublishedPermission();
  }

  /**
   * Test access when logged out.
   */
  public function subTestLoggedOut() {

    $this->drupalGet('/en/node/' . $this->node1->id());
    $this->assertSession()->statusCodeEquals(Response::HTTP_FORBIDDEN);

    $this->drupalGet('/en/node/' . $this->node2->id());
    $this->assertSession()->statusCodeEquals(Response::HTTP_FORBIDDEN);

    $this->drupalGet('/en/node/' . $this->node3->id());
    $this->assertSession()->statusCodeEquals(Response::HTTP_FORBIDDEN);

    $this->drupalGet('/fr/node/' . $this->node1->id());
    $this->assertSession()->statusCodeEquals(Response::HTTP_FORBIDDEN);

    $this->drupalGet('/fr/node/' . $this->node2->id());
    $this->assertSession()->statusCodeEquals(Response::HTTP_FORBIDDEN);

    $this->drupalGet('/fr/node/' . $this->node3->id());
    $this->assertSession()->statusCodeEquals(Response::HTTP_FORBIDDEN);
  }

  /**
   * Test access when logged in.
   */
  public function subTestLoggedIn() {

    $this->drupalLogin($this->user);

    $this->drupalGet('/en/node/' . $this->node1->id());
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);

    $this->drupalGet('/en/node/' . $this->node2->id());
    $this->assertSession()->statusCodeEquals(Response::HTTP_FORBIDDEN);

    $this->drupalGet('/fr/node/' . $this->node1->id());
    $this->assertSession()->statusCodeEquals(Response::HTTP_FORBIDDEN);

    $this->drupalGet('/fr/node/' . $this->node2->id());
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);

  }

  /**
   * Test access when translation is unpublished.
   */
  public function subTestUnpublished() {
    $this->drupalLogin($this->admin);
    $this->drupalGet('/admin/people/permissions');
    $this->submitForm([
      'authenticated[view any article en content]' => TRUE,
      'authenticated[view any article fr content]' => TRUE,
    ], 'Save permissions');
    $this->drupalLogout();

    $this->drupalLogin($this->user);
    $this->drupalGet('/en/node/' . $this->node3->id());
    $this->assertSession()->pageTextContains('Node three');
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);
    $this->drupalLogout();

    $this->drupalLogin($this->admin);

    // Change publication status.
    $this->drupalGet('/en/node/' . $this->node3->id() . '/edit');
    $this->submitForm([
      'status[value]' => FALSE,
    ], 'Save (this translation)');

    $this->drupalGet('/fr/node/' . $this->node3->id() . '/edit');
    $this->submitForm([
      'status[value]' => TRUE,
    ], 'Save (this translation)');

    $this->drupalGet('admin/reports/status/rebuild');
    $this->submitForm([], 'Rebuild permissions');

    $this->drupalLogout();

    $this->drupalLogin($this->user);

    $this->drupalGet('/en/node/' . $this->node1->id());
    $this->assertSession()->pageTextContains('Node one');
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);

    $this->drupalGet('/en/node/' . $this->node3->id());
    $this->assertSession()->pageTextNotContains('Node three');
    $this->assertSession()->statusCodeEquals(Response::HTTP_FORBIDDEN);

    $this->drupalGet('/fr/node/' . $this->node1->id());
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);

    $this->drupalGet('/fr/node/' . $this->node3->id());
    $this->assertSession()->pageTextContains('Nodule trois');
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);
  }

  /**
   * Test access when translation is unpublished and user has permission.
   */
  public function subTestUnpublishedPermission() {
    $this->drupalLogin($this->admin);
    $this->drupalGet('/admin/people/permissions');
    $this->submitForm([
      'authenticated[view any unpublished content]' => TRUE,
    ], 'Save permissions');
    $this->drupalLogout();

    $this->drupalLogin($this->user);

    $this->drupalGet('/en/node/' . $this->node3->id());
    $this->assertSession()->statusCodeEquals(Response::HTTP_OK);
    $this->assertSession()->pageTextContains('Node three');

    // Test access for anonymous user.
    $this->drupalLogout();
    $this->drupalGet('/en/node/' . $this->node3->id());
    $this->assertSession()->pageTextNotContains('Node three');
    $this->assertSession()->statusCodeEquals(Response::HTTP_FORBIDDEN);
  }

}
