CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------

This module enables permissions "View own content" and "View any content" for
each content type per language on permissions page.

The permissions lists will be very long, if you have many languages and content
types.

It is an extended version of Node View Permissions module


REQUIREMENTS
------------

None


INSTALLATION
------------

Install the Node View Language Permissions module as you would normally install
a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the Node View Language
       Permissions module.
    2. Navigate to Administration > People > Permissions > Node View Language
       Permissions to allow every content type for each language
    3. Navigate to Administration > Reports > Status Report > Node Access
       Permissions > Rebuild permissions


MAINTAINERS
-----------

 * sleitner - https://www.drupal.org/u/sleitner
