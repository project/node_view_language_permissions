<?php

namespace Drupal\node_view_language_permissions;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\NodeType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class definition.
 *
 * @category NodeViewPermissionsLanguagePermissions
 *
 * @package Access Control
 */
class NodeViewLanguagePermissionsPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a new NodeViewLanguagePermissionsPermissions instance.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(
    LanguageManagerInterface $language_manager,
  ) {
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('language_manager'));
  }

  /**
   * Permission function.
   *
   * Added the permissions.
   */
  public function permissions() {
    $permissions = [];
    $nodeTypes = NodeType::loadMultiple();
    /** @var \Drupal\node\Entity\NodeType $nodeType */
    foreach ($nodeTypes as $nodeType) {
      /** @var \Drupal\Core\Language\Language $lang */
      foreach ($this->languageManager->getLanguages(LanguageInterface::STATE_ALL) as $lang) {
        $lang_id = $lang->getId();

        $permission = 'view any ' . $nodeType->id() . ' ' . $lang_id . ' content';
        $permissions[$permission] = [
          'title' => $this->t('<em>@type_label</em>: View any content',
            ['@type_label' => $nodeType->label() . ' ' . strtoupper($lang_id)]),
        ];
        $permission = 'view own ' . $nodeType->id() . ' ' . $lang_id . ' content';
        $permissions[$permission] = [
          'title' => $this->t('<em>@type_label</em>: View own content',
            ['@type_label' => $nodeType->label() . ' ' . strtoupper($lang_id)]),
        ];
      }
    }
    return $permissions;
  }

}
